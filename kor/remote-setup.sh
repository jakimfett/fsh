#!/usr/bin/env bash
#
# This is a prototype, use discouraged.

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -Eeuo pipefail
else
    echo "These scripts require 'bash' version 4.4 or greater, your version is insufficient:"
	 bash --version
	 exit 13
fi

shopt -s nullglob globstar

# enable alias expansion, (disabled) load the aliases file
shopt -s expand_aliases
# && . "core/$(basename $(realpath ${1}))"
# if [ -f "kor/$(basename $(realpath ${1}))" ] && [ "$(basename ${1})" == "aliases.src" ];then
#logThis "Working directory is '$(pwd)'..."

# remote connection setup
hostName='bridge.local'
userName='user'

userDirs=('src' 'run')

for dir in ${userDirs[@]}; do
	echo ${dir}
done

function lastRun {
	echo "Successmuffins"
	sleep 3
	exit 0
}

trap lastRun EXIT



