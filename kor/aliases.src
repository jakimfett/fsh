# @todo - enumerate bash aliases before/after load, publish diff as alises.provided?

# @todo wouldn't this make more sense in the shim?
# @todo -> all files need execute protection
# @todo -> but all the variable/statestuffs needs to be in the shim.
# @todo and EXPECIALLY the shim checker! That definitely needs to be in the shim!
# self-containuity XD


# Open a tileable, disconnectable window session.
# Uses seconds since the unix epoc as the workspace differentiation postfix.
# command fails if ~/session directory fails to exist, @todo check and remediate
alias twm='dtach -A ~/run/twm.default.session -r winch dvtm'
# additional tiled workspaces queue needed, @todo
# @todo - date-based runfile
#alias twm+='dtach -A ~/run/twm.$(date +"%s").session -r winch dvtm'
alias twm2='dtach -A ~/run/twm.secondary.session -r winch dvtm'

alias tre='cd ~/run/tor-browser/ && ./start-tor-browser.desktop'

# Multitasking is an essential part of a broken workflow.	# <-- philosophy
# Do one thing at a time via multiple viewports.
# The terminal windows are portals to different parts of the machine.

# status printout
alias gitremote="git remote -v | awk '{print \$1, \" =-->  \",\$2}' | sort | uniq"
alias fUrl='echo "repository urls:";gitremote'

# courtesy of https://stackoverflow.com/a/2327216
alias cgrep="grep --color=always"


alias flg='cd ~/run/changelog/ && nano changes.md'

alias gitshort='fUrl;echo;git status -s'
alias gitfull='fUrl;echo;git branch -vv;echo;git status --short --column;echo;echo "current working directory:";echo;pwd;ls -ah;echo'

# originally 'gst', changed to avoid conflict with gnu smalltalk
# briefly was 'fit', but tab-completion collisions discouraged that.
#
alias f='git' # <-- this may break things horribly if someone else decides to use 'f' for a command/utility >_>
#
# still kinda gets pronounced like "eff it", so the original humor-purpose is intact.
alias fet='clear;gitfull'

# for when you really gotta see *all* the differences in your project, staged or not
alias dc='git diff && git diff --cached'

# fixing application database issues in evolution mail on mint 19.something
alias feq='evolution --force-shutdown'

# alias flint="echo shellcheck \$(find . | egrep \"\.sh|\.src\") | less"

# Give the non-machines a readable default time format.
#alias when='date +"%A, %d %B%t(Day %j, in %Z/GMT%z)%n%n%t%Y_%m_%d @ %H.%M.%S"'
# @todo - autogenerate aliases from stacks in ./stk/
alias when="${fDir}/stk/when.sh"
# @todo - merge with tool/when.sh <-- done, right? Put this in @review


## QoL shortcuts ##
# keep your shortcuts close, your aliases, closer
alias pathadd='export PATH="$(realpath `pwd`):${PATH}"'
#alias fshfix="export pwd=$(pwd) && cd ~/run/fsh/ && . shim.src && cd "${pwd}"

#alias dl=
alias sd='ssh 192.168.88.1 "system shutdown" && echo router shutdown; sleep 5 &&shutdown -hP now'
alias nano="nano --nowrap --backup --positionlog --autoindent --linenumbers --softwrap --tabsize 3"
alias gg='sudo su --login'
alias micloop='pactl load-module module-loopback'
alias micunloop='pactl unload-module module-loopback'
alias kd='killall Discord;killall Discord;killall Discord;killall discord;'

### Useful SSH-adjacent aliases ###
# patchwork'd from https://unix.stackexchange.com/a/349821
alias ssh-showkey='ssh-keygen -lf <(ssh-keyscan localhost 2>/dev/null)'
alias ssh-mykey='cat ~/.ssh/id_ed25519.pub'
alias ssh-makey='ssh-keygen -a 256 -t ed25519'

# Debian sysadmin alias(es)
alias update="sudo apt update; sudo apt install -f -y; sudo apt autoremove -y"
alias uprepair="update; sudo apt install --fix-broken; sudo apt install --fix-missing"
alias upgrade="update; sudo apt dist-upgrade -y;uprepair"
alias shutgrade="upgrade; sudo shutdown -hP now"
alias regrade="upgrade; sudo reboot now"

## making git less obtuse ##

# fetch all the things?
# @todo - automate all remotes?
# @todo - shelve/restore remotes
# @todo - subcommand consistency audit
# @todo - featureset analysis
# @todo - build pipeline & automation
# @todo - git-lite proposal

# because qwerty is problematic -  https://skeptics.stackexchange.com/a/5154|https://archive.today/I5Z6Z

# @todo - autocomplete for keys adjacent? Need to calculate ramifications of fuzzy command completion thoroughly first, tho.
alias got='git'
alias exut='exit'

# courtesy of https://social.wxcafe.net/@wxcafe/103620041773395685
alias lsd='ls'


# Know your priorities
# ...courtesy of Kanban.bash (CoderOfSalvation)
#
# @todo - put the variables in one place?
# inline is super convenient tho
#
# @todo - manage kanban.bash better, see above for specifics
# @todo - statuses migration...sed -i perhaps?
# @todo - can nano edit more than one thing at a time?
# @todo - fix all the magic locations here, these can be local variables in a function, too
# @todo standardize this kanbash config stuffs better.
# too many wobbly turtles.

# this chunk is so crufty, @todo document & standardize
# good thing it's all getting yarded out to test taskwarrior.
# 2021038 - previous comment aged poorly, probably migrating back to kanbash XD

# @todo migrate a bunch of these, but this one specifically, to functions
# @migrated alias ff="cd ${fDir} && gitshort && khere"
# @todo eliminate the magic variables

# display what discs are connected, without loopback spam
alias show-disk='lsblk -p | grep -v loop'


# @todo - break aliases/functions into appfiles

# rebuild nixos
alias nrb='nixos-rebuild test && nixos-rebuild switch' # only 'switch' if 'test' is successful

# tasking aids
alias t='task'


# colorized ls
if [ -z ${LS_OPTIONS+set} ]; then
	alias lf='ls $LS_OPTIONS -B'
	alias ll='ls $LS_OPTIONS -lah'

	# using '-t' for time-sorted listing of files
	alias tl='ls $LS_OPTIONS -Blaht' # <-- aka 'time list'
else
	alias lf='ls -B'
	alias ll='ls -lah'

	alias tl='ls -laht'

fi
