#!/usr/bin/env bash

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -Eeuo pipefail
else
    echo "this script helper requires bash v4.4+, your version is insufficient"
	 bash --version
	 exit 13
fi

shopt -s nullglob globstar expand_aliases

#declare -A envConf

buildDir="~/run/tmp"
toolDir="~/run/fsh/tool/"
loopWait='5'

clear
echo "${envConf[mkDirs]}"

eval when.sh
echo
echo

if [ ! -d "${buildDir}" ]; then
	echo
	echo "...cannot find build directory."
else
	systemctl status laminar.service
fi

echo

if [ ! -f "~/run/fsh/tmp/loop.run" ]; then
	read -r -p "$(echo -e "input:> ")" response

	echo "Executing: '${response}'"
	echo
	eval "${response}"
else
	echo
fi


function cleanup {
	echo done
	sleep "${loopWait}"
	~/run/fsh/quick.sh && exit 0
	echo "...loop failed to execute!"
}

trap cleanup EXIT
