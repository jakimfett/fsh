# aggregate all @todo references


# unsorted
## === # === ##
# @todo - item  

# @todo - read-only host/infrastructure mode, tmpfs for users?
@todo - hardware shutdown button scripting, powerloss fastshutdown, supercap grace duration, brownout point, etc testing

@todo - compile+add archive/wayback links & [link](url|url|...) compiletime replacement w tested-valid link logging.
@todo - create manpage ([*](https://liw.fi/manpages/)[*]())
# @todo - process lines beginning/ending in '#'
# @todo - scrape all @todo instances during compile.
@todo - logging levels refactor  
@todo - install/setup rundir && bin link
@todo - commercial use & export authorization doc signables  
@todo - bridge group config (pihole, tor-bridge, ssb-server-lite, local feedback aggregator)  
@todo - purge when.sh history (see https://github.com/newren/git-filter-repo/ ), it's been moved to the aliases file
@todo - autoinstall git-filter-repo, see previous
@todo - fix logThis priority, verify/document that default (3) looks reasonably nice.
@todo - protect against accidental override of other similarly named system tools, eg failover/integrate with/check [git-flow-hooks' functions.sh](https://github.com/jaspernbrouwer/git-flow-hooks/blob/master/modules/functions.sh) functionality.

# @todo - review https://www.gnu.org/software/bash/manual/html_node/Invoking-Bash.html capabilities
#
# Thanks to shellharden for the bash boilerplate:
#	https://github.com/anordal/shellharden/blob/master/how_to_do_things_safely_in_bash.md#how-to-begin-a-bash-script

# @todo get updated hashbang, auto-replace header/footer in all .sh scripts

# @todo - conditional `set -x` on loglevel > 3? ref:set -euo pipefail

@todo - references auto-downloader, updater, versioner.
