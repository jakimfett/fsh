Text editing, and by extension programming, has always been a [hairy problem](http://www.jargon.net/jargonfile/h/hairy.html).

# Core Premise
At the core, fsh/functions.sh, the [DarkPi project](https://darkpi.com/), and even my career as a system administrator, has been in pursuit of a technology experience that doesn't suck for both administrator and end user (along with everyone inbetween).

The `fsh` stack is intended as a fulcrum, a force multiplier, which can be combined, remixed, and piped around. Stacking with other types of optimization or efficiency multipliers is encouraged and intended, among them NixOS, Named Data Networking (via Yggdrasil), Mosh, Laminar, Nginx, and a host of other lightweight, multipurpose tools.

Because ultimately, this is about making better tools.

Tools that suck less.

## All Tech Sucks Right Now
Live in westernized culture requires engagement with problematic (fragile/coercive/) user experiences.

The platform that isn't being compensated sufficiently is the platform that seeks your identity, your metrics, your marketability for their coffers, and only value your privacy or utility insofar is it benefits the bottom line.

They create systems of feedback optimized to hear the right metrics, and then they turn the thumbscrews, ignoring the cries of the users who fall through the cracks. Still expected to experience the tech benefits and use, but unsupported, unhelped, unheard.

There's no more innovation in basic human-computer interface than the vertical mouse, for all practical purposes.

Computing "advances" are just circlejerks to prove you can make silicon scream the highest.

Not a fan.

Not efficient, or tasteful, or useful besides gaming or benchmarking, and those niches are...saturated, to say the least.

## Context
To an automation engineer, there's a lot of stuff that can be replaced with a few lines, or a few dozen/gross/peck/wtfever lines, of shell script.

There's a lot that could be automated, fixed and functionally forgotten, past the point of push-button.

The end user won't care about most of the options. They want the three-to-six workflows that get them what they desire from their device, and they'll do those routines until someone pries it away from them. If you doubt this, do six weeks of helpdesk for a call centre, and then we'll talk.


Remove everything unnecessary.

...or said another way, "Do one thing well."

## Tech Could Be Beautiful
There's a way to make objects that feel nice in your hands, and flat rectangles aren't it for a fairly significant portion of the sentient life on this planet.

Your device rests easily, naturally, in your hands, and the interface is intuitive, flexibly structured, and consistent.

Contact points give you multi-touch commands, and kinesthetic tactile interaction comes with use. Traces on flat surfaces beneath your fingertips, the lights you control dancing with the beat of your heart. Moving with music from room to room, your consent required to monitor your heartbeat and mix with the lights. All of it working, just the way you set it up last year, but it's solid tech, meets your needs, and upgrades keep it alive in the small ways you decide to tweak it.

## Tech Could Be Fun
We learn better through play.

Tech can facilitate that.

It doesn't.

But it could, and the hardware around the software we're building here is intended to facilitate that. Working together, as a group, requires a willingness to have fun together, and our devices could connect us no matter where we are in meatspace.

In addition to the functional requirements of the secure_collaboration document, this project intends to facilitate hosting a MineTest (or similar sandbox) game for collaborative fun and downtime.

## Tech Could Be Easy
Because [Jessica](https://swiftonsecurity.tumblr.com/post/98675308034/a-story-about-jessica), and your grandparent, really need it to be.

# Solution
???

Build a better stack?

@todo - write a conclusion?

## Two Simple Hacks
The functions.sh system, and by extension the `fsh` implementation, is made possible by two bits of bash-ish ability.

First, there's the `.bash_aliases` shim. This autosources the `fsh/core/aliases.src` file when a shell is loaded, which allows the use of development tools like `eba` to edit the bash aliases file, or `xtrace` to peek behind normally-opaque objects.

Second, the use of bash's exit trapping capabilities (via a line to `core/loop.sh` in `aliases.src`, default disabled) to reload the terminal window after each command, making each folder a sort of "current desktop" that takes direct commands.

These are both (intended) to work with the `twm` (tiling window manager, reconnects) `twm2` (twm, but spawns new instances, reconnecting is nontrivial) commands (capabilities will be merged eventually).

### Bugs
Many.

Of which most are hiding at the moment.

#### `twm` / `twm2` scrollback
One of the "features" of the underlying `dtach` / `dvtm` stack is the lack of scrollback.

That said, a limited amount of scrollback, with something more cohesive as logging than "grep your .bash_history file and figure it out from the logs", can make it a somewhat problematic fave so far as remote-management goes.

(buried-ish lede) the point being, `twm` keeps ssh/mosh/etc disconnects from eliminating your in-progress terminal work, and the use of file-based session management means it can cross hardware boundaries, probably.

## Okay, what now?
Well, the microsite is (probably) down, so you'll have to find the latest repo among github, gitea, and our various development machines.
