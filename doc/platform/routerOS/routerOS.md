there has to be a way to learn this with the resources at hand.

# First Hours
I've learned bits and pieces of the Mikrotik RouterOS system,  
and rather a lot of it has 'Nix roots,  
so there's groundwork laid.

Several existing partial attempts at an "autoconfig" script exist,  
they just need combined, condensed, and rendered efficient.

First priority is a window outside.  
Apparently, that's already set up.  
Creating a settings export for posterity.

## Minutes Later:
Soft-bricked again.

Trying to bridge the connection between LAN ports, this time.

No idea how I've kept my shell connection with the microserver.

Taking some time to see if the login problems over there can be solved,  
before tackling the MikroTik problem again.

...am I just delaying?
we'll never truly know...

## MikroTik: Exports
The `export` command from the RouterOS command line produces a line-by-line configuration commands printout. In theory, the system could be wiped, and then the delta between "default" settings and the local backup could be applied, but questions of "what to wipe" and "what to overwrite" and whether having multiple valid configurations will break things has so-far prevented attempts to actually test the backups.

There was documentation of the necessary system-reset-level (via a pinhole button wait time mechanism) for getting a "clean slate", but it's rather easy to softbrick this system, and so far getting it to work seems half luck, half chance, half repeating the gui options until they "stick" in memory.

Have heard, via comments in forums etc, that the command line experience is much...less broken.

Time will tell.

## Another Attempt
Fresh NixOS install in the mix, attempting to get an outside connection.

Softbricked the router again, unsure how but suspect the update button.
This always seems to be where we get stuck.

Each time I use quickset to apply anything, it softbricks the router. As soon as I hit "apply" I knew it was a mistake. Specific option changed was "Address Acquisition", but I suspect it's one of the others where the default results in a softbrick. Third try, now. Going to attempt to set this up without actually hitting "apply" this time.


Reset router by holding reset pin/contact/button for 5-7 seconds (until green steady light blinks), then wait for DHCP on a non-zero ethernet port.

Run these commands on a LAN ethernet connected system:
```
# Remove outdated host keys (assumes router was softbricked and factory reset)
ssh-keygen -f ~/.ssh/known_hosts -R "192.168.88.1"
# Grab the existing config and stash it locally.
ssh admin@192.168.88.1 "export" > default.mikrotik.cfg
```
Navigate to the http interface, find interface address with `ip a`.
Open address in browser, top right dropdown select "CPE" mode.
Select desired network, connect.

This is as far as success is known.

Several critical bits from https://medium.com/dac-technology-blog/connect-mikrotik-to-an-open-wifi-network-and-provide-internet-over-lan-and-wlan-1c0ffb033ac5, combined with https://forum.mikrotik.com/viewtopic.php?p=712335#p763654 and https://wiki.mikrotik.com/wiki/Manual:Wireless_Station_Modes#Mode_station-pseudobridge and https://www.reddit.com/r/mikrotik/comments/4rdbgs/dhcp_over_a_wireless_bridge_link/ and http://archive.vn/pmW39

both bridge and dhcp client must be set to `wlan` for dhcp to work on the ethernetworked devices.
