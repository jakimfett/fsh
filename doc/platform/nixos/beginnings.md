You'll need the system image:
https://hydra.nixos.org/search?query=sd_image
(see nixos-dl.sh)

And something to flash it to an SD card with:
https://github.com/balena-io/balena-cli
(courtesy of https://forums.balena.io/t/command-line-option/7171/8, requires nodejs? ugh.)

Flashing to an SD card is nontrivial, sadly.
https://www.tecmint.com/linux-compress-decompress-bz2-files-using-bzip2/

You'll need to extract the file, then verify it (see "details" on the hydra page for the md5 etc)

@todo - figure out the 'bs' arg.
something like 'dd bs=4M status=progress conv=fsync if=./imagefile of=/path/to/device'
This gives you a (simple) status readout as the file writes, and syncs the underlying disk after (conv=fsync)

The distinction between:
`wget https://hydra.nixos.org/build/135337873/download/1/nixos-sd-image-20.09.2653.fee7f3fcb41-aarch64-linux.img.zst` (2.8GB)
AND
`wget https://hydra.nixos.org/build/135341821/download/1/nixos-sd-image-20.09.2653.fee7f3fcb41-aarch64-linux.img.zst` (2.74GB)
is the difference between a non-booting, and a working, system.

