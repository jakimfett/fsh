#!/usr/bin/env bash
#
# Description of the file, from a meta-perspective.
# As a template, this file also acts as the post-commit hook?
# (install: `ln -s ../../doc/template.sh .git/hooks/post-commit` from fsh maindir.)
#
# usage: doc/template.sh [-h | --help] =--> does nothing, currently?



# per usage in debian, perhaps using:
# if [ -z ${VARIABLE+set} ]; then
# to determine variable state would be recommended?
#
# Need to confirm that this won't mess with bash version checking to use here,
# but there's use for checking envConf (& other globalish vars),
# and for action/feature flags too.
#
if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    set -Eeuo pipefail
else
    echo "this script helper requires bash v4.4+, your version is insufficient"
	 bash --version
	 exit 13
fi


shopt -s nullglob globstar

### END HEADER ###

# Pre-emptive variable declaration
declare -A envConf command param

# verbosity, min 0, max 9.
envConf['verbosity']=3

## include f.sh ##
# @todo - pull header, footer, fsh invocation from .part (?) files for autocompile
#shopt -s expand_aliases;source "path/to/f.sh";
## end includes ##

command['template']="command & usage info."
envConf['description']="Template program, boilerplate mostly."
