The meta is simple.

# Facilitate Thy User(base)
Understanding and wisdom are oft separated by time and/or space.

Accessibility in isolation must drive basic considerations.

To this end, many convoluted attempts at beginning, middle, and end may be attempted,
yet never truly finished with the final polish.

## Tenant One
Installation must be simple.

Download image, burn image, run.

More than three steps, and massive falloff in accessibility is evident.

Your userbase needs simple.

Make it so.

`install.sh`

## Tenant Two
Maintenance must be simple.

A thousand levers to accomplish one job is less efficient than a single manual task.

Automate all non-critical decisions, and allow decision-level variations beyond the boolean.

`upgrade.sh`

## Tenant Zero
Removal must be simple.

Preserve your user's data beyond program end.

`remove.sh`

# === ### === ##

When possible, shorten the control feedback loop.

These scripts aspire to be examples of beautiful programming,
examples of system engineering of the functionally artistic sort,
mindful of [mel's kōan](http://www.catb.org/jargon/html/story-of-mel.html)
[*](https://archive.today/f46RR) and the marathon.

write for the code that could be ideal
automate the minimum viable path

do things for more than one reason.


Regretfully, programming is complex, so please (also) read the warning labels.
