If you're not developing,  
this is just technobabble.

If you are planning to develop,  
seriously [grok](http://catb.org/jargon/html/G/grok.html) these docs for an easier experience.

## Format
Code goes in `backtics` (SHIFT+tilde on a QWERTY keyboard),  
parameters go inside angle brackets `<like/> <so/>`(,
parentheses used to indicate optional segments).

blocks use `#instruction#;bashcode;#noitcurtsni#` nomenclature, eg:
```
#setup#
mkdir -p $(realpath ~/)project/fsh
#putes#
```

This allows us to parse looking for lines that both end & begin with a hashmark, and then pattern match the inverted string against future similarly-bookended lines (probably using `perl` because [speed](http://rc3.org/2014/08/28/surprisingly-perl-outperforms-sed-and-awk/))

### Conditionals
Dependencies can be met in more than one way.

`GCC:4.9||Clang:3.5`  
and  
`apt||apt-get`  
are both examples of this.

## Security
Bash has nuance, and the [pitfalls](http://mywiki.wooledge.org/BashPitfalls) are many & varied, listen to someone [competent](https://github.com/anordal/shellharden/blob/master/how_to_do_things_safely_in_bash.md) on the subject ([*](http://archive.ph/sfYp2)).

Safe is fast, y'all.
