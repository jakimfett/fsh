Priorities, priorities...scrapyards are such sweet sorrow.

# Scope Creep

https://danluu.com/cli-complexity/

Originally, this was a tool for abstracting a single command.

It has grown over the years, but somewhere there's good engineering buried inside this pile of technical debt and acutely focused frustration.  
This is an interface, and interfaces for general usage need to be simple.

Simple tools have many uses.  
LSH taught me that, thank you good sir.

Also, there's much to be said for keeping track of the complexity, 


# setting up your hardware

Use a [GUID Partition Table](http://dcjtech.info/topic/types-of-partition-tables/).

Either `ext4` or `xfs` filesystem type is [encouraged](https://docs.centos.org/en-US/8-docs/advanced-install/assembly_partitioning-reference/), though the goal is to use `zfs` eventually/soon.

@todo - set up standard partitions in build config file



# tracking tasks

A simple solution is [Kanban.bash](https://github.com/coderofsalvation/kanban.bash).

Using [watson](https://tailordev.github.io/Watson/) is also an option, though the name intersection with a megacorp offering makes discovery around features nearly impossible.

Currently testing [taskwarrior](https://taskwarrior.org/).

---

unsorted, without attempt at order, but some attempt at grouping:

# networking

https://superuser.com/questions/370965/how-can-i-check-which-ports-are-busy-and-which-ports-are-free-on-my-linux-machin
https://blog.fpmurphy.com/2015/02/ip-dynamic-port-range.html
https://tools.ietf.org/html/rfc6335

https://www.theurbanpenguin.com/find-my-raspberry-pi-using-nmap/
https://phoenixnap.com/kb/nmap-scan-open-ports

# un-git

https://github.blog/2016-02-01-working-with-submodules/
https://stackoverflow.com/a/12222225

https://stackoverflow.com/a/18881044
https://git-scm.com/docs/gitattributes

https://flummox-engineering.blogspot.com/2015/05/how-to-check-if-file-is-in-git.html

# golang
https://stackoverflow.com/questions/51397328/how-to-install-go-package-manually-from-source-code
https://golang.google.cn/doc/install/source

# bash

# to check for presence of a string in a given textfile, as a conditional:
# if ! grep -q "string" "file.name"  ; then

Horrible things can be done with exit trapping. [Horrible. Beautiful. Horrible](https://www.reddit.com/r/pomodoro/comments/a0897q/a_pomodoro_timer_for_the_shell_works_with/eiy297h).
```
# trap ctrl-c and call ctrl_c()
# handle ctrl-c to stop ongoing task
trap ctrl_c INT
function ctrl_c() {
  echo "Quit ongoing task: $1"
  task $TASK stop
  exit 1
}
```



https://stackoverflow.com/a/3130425
https://www.cyberciti.biz/faq/redirecting-stderr-to-stdout/

https://linuxize.com/post/fdisk-command-in-linux/
https://www.brianstorti.com/stop-using-tail/

https://stackoverflow.com/questions/2500436/how-does-cat-eof-work-in-bash
https://en.wikipedia.org/wiki/Here_document#Unix_shells

https://stackoverflow.com/a/27875395

https://www.cyberciti.biz/faq/unix-linux-bash-script-check-if-variable-is-empty/
https://linuxize.com/post/bash-check-if-file-exists/

https://www.cyberciti.biz/faq/how-to-display-countdown-timer-in-bash-shell-script-running-on-linuxunix/
https://ss64.com/bash/exit.html
https://www.networkworld.com/article/3574960/using-bashs-shopt-builtin-to-manage-linux-shell-behavior.html

https://www.tecmint.com/sort-ls-output-by-last-modified-date-and-time/

Time in bash - https://stackoverflow.com/questions/10990949/convert-date-time-string-to-epoch-in-bash

# nixos
So far, there's a lot of stuff that doesn't quite work right, yet.
A lot of it seems to be hardware related, as it primarily has affected the sub-1GB-RAM models of the Raspberry Pi.
Both commands that create a hang/crash are in `nix-env`, the `-i` and `-qaP` options, specifically.
https://nixos.org/guides/ad-hoc-developer-environments.html
@todo - create automated testing for main nix-esque commands?

https://discourse.nixos.org/t/howto-setting-up-a-nixos-minecraft-server-using-the-newest-version-of-minecraft/3445
https://gitlab.muc.ccc.de/txpower/nixpkgs/commit/888d01da4811c5ebc1c6a841c6b475d5b6224098#7d70105447ed1dbd8d09c92cbc993a22207156b4
https://spartanengineer.com/nixos/2017/09/25/basic-git-server-with-nixos.html

https://nixos.org/guides/nix-pills/why-you-should-give-it-a-try.html
https://www.foxypossibilities.com/2018/02/04/running-matrix-synapse-on-nixos/
https://nixos.wiki/wiki/NixOS_on_ZFS

https://jin.crypt.sg/files/nixos-deep-dive.pdf
https://hands-on.cloud/why-you-should-never-ever-use-nixos/
https://nixos.org/guides/nix-pills/index.html

https://nixos.org/manual/nixos/stable/#sec-changing-config
https://rycwo.xyz/2019/01/29/nixos-series-configuration-primer
https://stephank.nl/p/2020-06-01-a-nix-primer-by-a-newcomer.html
https://blog.stigok.com/2020/03/07/nix-and-nixos-notes.html

https://rbf.dev/blog/2020/05/custom-nixos-build-for-raspberry-pis/#nix-packages-and-image-configuration
https://nixos.wiki/wiki/Configuration_Collection

https://www.vultr.com/docs/install-nixos-on-vultr
https://asymmetric.github.io/2019/12/21/nixos-rebuild/
https://www.youtube.com/playlist?list=PLRGI9KQ3_HP_OFRG6R-p4iFgMSK1t5BHs
https://www.reddit.com/r/NixOS/comments/5rv5nt/how_to_manage_user_space/

https://nixos.org/manual/nixos/stable/index.html#sec-booting-from-usb
https://nixos.org/manual/nixos/stable/index.html#sec-wireless
https://nixos.wiki/wiki/Bluetooth


https://github.com/NixOS/nix/issues/1981
https://unix.stackexchange.com/questions/557977/how-to-clone-a-private-git-repository-in-a-nix-derivation/558401#558401
https://www.reddit.com/r/NixOS/comments/g8c734/how_to_clone_a_repository_at_a_specific_location/fomus22/?context=3
https://github.com/NixOS/nixpkgs/pull/47842
https://discourse.nixos.org/t/setting-up-path-for-system-activationscripts/4034

https://github.com/NixOS/nixpkgs/issues/8567
https://discourse.nixos.org/t/systemd-service-from-flake/9135
https://discourse.nixos.org/t/difference-between-fetchgit-and-fetchgit/3619
https://github.com/NixOS/nix/pull/3216
https://www.reddit.com/r/NixOS/comments/bqt4iq/optionally_deriving_from_local_source_or_from/
https://github.com/NixOS/nixpkgs/issues/42608
https://git.yori.cc/yorick/nix-conf/src/commit/5a5a234fd9b0501189695af7aea98a1ec1dbd111/modules/muflax-blog.nix#L6
https://github.com/NixOS/nixpkgs/issues/42608#issuecomment-400274790


https://www.reddit.com/r/NixOS/comments/6zn5tv/manage_nixos_config_from_git_without_having_to/

https://github.com/NixOS/nixpkgs/issues/25262

https://github.com/nix-community/nixos-generators


## raspi
https://www.raspberrypi.org/documentation/installation/installing-images/

https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi_3#Camera
https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi#HDMI_output_issue_with_kernel_5.4_.28NixOS_20.03_or_NixOS_unstable.29

https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi_3#Audio
https://www.raspberrypi-spy.co.uk/2013/06/raspberry-pi-command-line-audio/

https://github.com/NixOS/nixpkgs/issues/82455
https://github.com/raspberrypi/rpi-imager/issues/41#issuecomment-602717427

## sdcard writing

https://forums.balena.io/t/command-line-option/7171/8
https://fight-flash-fraud.readthedocs.io/en/latest/introduction.html
https://github.com/raspberrypi/documentation/blob/master/installation/installing-images/linux.md#optional-checking-whether-the-image-was-correctly-written-to-the-sd-card
https://citizen428.net/blog/installing-nixos-raspberry-pi-3/

# systemd

https://linuxconfig.org/how-to-create-systemd-service-unit-in-linux
https://unix.stackexchange.com/questions/139513/how-to-clear-journalctl
https://stackoverflow.com/questions/37585758/how-to-redirect-output-of-systemd-service-to-a-file
https://www.linux.com/training-tutorials/writing-systemd-services-fun-and-profit/
https://www.linux.com/training-tutorials/systemd-services-beyond-starting-and-stopping/
https://unix.stackexchange.com/questions/225401/how-to-see-full-log-from-systemctl-status-service

# fdisk

https://andrey.mikhalchuk.com/2008/02/12/how-to-automate-fdisk.html

https://serverfault.com/questions/258152/fdisk-partition-in-single-line#comment1354587_721878

# atom
https://flight-manual.atom.io/using-atom/sections/basic-customization/#customizing-language-recognition
https://flight-manual.atom.io/using-atom/sections/basic-customization/#finding-a-languages-scope-name
https://stackoverflow.com/questions/22363070/how-do-i-make-a-default-syntax-by-filetype-in-atom-text-editor#32508353

# cron
https://github.com/NixOS/nixpkgs/issues/15373

# taskwarrior
Switching task contexts - https://steve.dondley.com/setting-up-multiple-project-areas-with-task-warrior/

# shellcheck
https://dev.to/david_j_eddy/using-shellcheck-to-lint-your-bashsh-scripts-3jaf
https://github.com/koalaman/shellcheck#in-your-build-or-test-suites

# ssh

http://www.softec.lu/site/DevelopersCorner/HowToRegenerateNewSsh


# unsorted
: Without attempt at order:


https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/

https://github.com/anordal/shellharden/blob/master/how_to_do_things_safely_in_bash.md
https://unix.stackexchange.com/questions/114908/bash-script-to-convert-all-flac-to-mp3-with-ffmpeg

https://stackoverflow.com/a/44782681

https://medium.com/@ayushya/move-directory-from-one-repository-to-another-preserving-git-history-d210fa049d4b

https://gist.github.com/ssp/1663093

https://www.cyberciti.biz/faq/show-progress-during-file-transfer/

https://jamesclonk.github.io/vultr/
https://www.vultr.com/api/

https://stackoverflow.com/questions/5735666/execute-bash-script-from-url#comment79462075_5735767


# down here is the heavy stuff
http://www.brucejmack.net/Guides/principals_and_patterns/Stateless%20Design%20Pattern.htm
http://ewontfix.com/14/
