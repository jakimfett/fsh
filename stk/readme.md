Stacks are something stable-ish, but minimal.

Oftentimes stretched far beyond their capabilities by ineffeciencies and fear of refactoring.

# Practicum

Technology, and especially code, can easily be stacked too high.

Like other text, you can use programming to support something else.  

Maybe giving a chair occupant extra height, or to stablize an off-balance table.  
But you can only stack so high before things become unbalanced again.

Stacks are good stopgaps, but perform poorly over time.  
Some stacks are scripts, and some scripts are stacks.  
There are healthy margins, and thirds are a good guide.  

Remember,  
your application is unique,  

programming is art,  
and that both perfection and flaws  
are important parts of progress.
