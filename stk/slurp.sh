#!/usr/bin/env bash
#
# Pulls bash history from (accessible) subfolders of a passed-in location.

# @todo - add bash header stuffs


path="${1}"
filePath="$(echo "${path}" | cut -d'/' -f 4).dir.idx"

sudoer=$(sudo -v 2>&1)


if [ ! -d "${path}" ]; then
	echo "Location '${path}' is not a directory."
elif [ ! -w "${path}" ]; then
	echo "Location '${path}' unwriteable by '${USER}'."
fi

# process being done this time
cmd="find '${path}' > '${filePath}'"
#cmd="find '${path}' > \"${filePath}\""

if [ ! -z "${sudoer}" ];then
command_string="ps ax"
process="ps ax"
eval "$command_string" | grep "$process"

sudo eval "${cmd}"
else
	eval "${cmd}"
fi

grep '.bash_history' "${filePath}"
