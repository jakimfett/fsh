#!/bin/bash -u
#
# usage:
# f --tool resizer "<path/to/folder/or/file.media"
#
# input - path to digital picture(s), resizes optimized for web/CDN use.
#
# output - an approximation of the original media, resized to <configurable/>
#

# @todo - implement this, deploy to laminar, integrate with seafile

exit 13
