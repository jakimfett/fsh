#!/usr/bin/env bash

if test "$BASH" = "" || "$BASH" -uc "a=();true \"\${a[@]}\"" 2>/dev/null; then
    # Bash 4.4, Zsh
    set -Eeuo pipefail
else
    echo "this script helper requires bash v4.4+, your version is insufficient"
	 bash --version
	 exit 13
fi

shopt -s nullglob globstar

declare -A envConf
envConf['mkDirs']="~/run/build ~/src ~/bin"

buildDir="~/run/build"
loopWait='5'

clear
echo "${envConf[mkDirs]}"

eval ~/run/fsh/tool/when.sh
echo
echo

if [ ! -d "${buildDir}" ]; then
	echo
	echo "...cannot find build directory."
else
	systemctl status laminar.service
fi

echo


function cleanup {
	echo done
	sleep "${loopWait}"
	~/run/fsh/quick.sh && exit 0
	echo "...loop failed to execute!"
}

trap cleanup EXIT
