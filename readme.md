Functions, shell (lite).

Pre-alpha, see `b25c535b582f8ecb2632e48b2c940565e05cc3d1` for roadmap.  
Development has diverged from roadmap, though goals are intact, path will differ.

This repo is mostly about automating a robust, minimal, sustainably sourced build pipeline.  
...though ultimately, this is a **bash aliases handler**, and should be treated as such.

# test

Get the shim:
`wget https://functions.sh -o shim.src`

OR  
Clone the repo:  
`mkdir -p ~/run/fsh && cd ~/run.fsh && git remote set-url fsh user@functions.sh:. && git fetch fsh dev && git checkout dev`

Source the shimfile:
```
. ~/run/system/fsh/shim.src
```

Works best in conjunction with a git project, try `fet` or `fd`, or `ff` to do fsh dev work.

# shimstall

Once you've sourced the shimfile, use `shimMe` to make it semi-permanent.

This will move any existing file at `~/.bash_aliases` to `~/.bash_aliases.bak`, sourcing that file if it exists, and putting a symbolic link from the contents of the current `fDir` variable in place of the aforementioned file.

In case that was as difficult to follow as it was to write, you'll end up with a symbolic link from `~/.bash_aliases` to wherever you've got `shim.src` stashed away, and any previous aliases will override any fsh defaults, to preserve customization continuity.

# quickstart

in an ideal world, this would look something like  
> `bash <(curl -s https://functions.sh/)`
>  
> This executes the bootstrapper in a subshell,  
you can start using `f` commands in new shells immediately.
> 
> Or,  
> if you're the cautious type,  
> you can clone, build, and install it after verifying the hash (see [doc/readme.md]).
>

currently, something akin to  
`mkdir -p ~/run/fsh && cd ~/run/fsh && git clone -b dev https://functions.sh/ . && . ~/run/fsh/shim.src`  
will probably work to get you testing, but your experience on the `dev` branch may vary wildly.

## Things Which May Work 
### (currently-2020.Q3-ish)

Chronostamp (`stk/when.sh`)
Loading the shim (`. shim.src`)
Viewing git summary (`fet`) <-- requires shim to be loaded and git (among other operational dependencies) to be installed.

The kanbash cluster works-ish, but requires manual install and config of [kanban.bash](https://github.com/coderofsalvation/kanban.bash).
Task tracking use migrating to taskwarrior (from kanban.bash), but continuied development and reswap once feature parity is achieved is encouraged.

## Things Which Most Likely Don't Work

...this comment "camelCase for variables, lowercase for functions" is an ideal, with low adoption.

The `xtrace` utility may break everything, or it may work for your use case. Know what you're using before you delve.

# persistence

The shim, as installed by the quickstart command, allows the use of aliases (see `core/aliases.sh`) and most `fsh/tool` scripts, but worry-free persistence needs the build service installed, and many commands are for system administration, eg `shutgrade`.

> Build service installation is a whole kettle of worms, and needs a proper install process hacked out.


# scripting
Include `f.sh` in your scripts with:  
`shopt -s expand_aliases; . path/to/f.sh`  
or execute piecemeal as described in [#quickstart](#quickstart)

# legal
all contributions assumed original & freely given,  
non-commercial/hobbiest/educational use encouraged.

## knowledge disclaimer
All of this came from a book or a website somewhere,  
if you believe you work has been referenced,  
thank you for teaching me that particular trick.

More than willing to add you to the list of referenced code, please feel free to open a pull request.

For purposes of branding & legality,  
`f.sh`/`fsh`/`f` &copy; 1990-present [@jakimfett](https://jakimfett.com/).

Due to the resurgence of public toleration for neo-nazi identities,  
authoritarian/nationalist/colonial use explicitly prohibited,  
go kick rocks in flip flops, ya gorram fascists.

# commercial support
Contact '[via email](mailto:office@assorted.tech)' or  '[web form](https://assorted.tech/contact)' for commercial licensing & professional use.



# old usage (for posterity)
Include functionality in your own scripts with:
```
#!/
DIR=path/to/fsh/
[[ -f $DIR/$1  ]] || { echo "fsh not found"; exit 1; }
[[ "$1" =~ ../ ]] && { echo "Forbidden path"; exit 2; }
source /usr/local/etc/library.sh
cd $DIR
launch_script $1
```
