Functions, shell (lite).

> currently, the dev branch works better than main.  
> Overall this is pre-alpha, see `b25c535b582f8ecb2632e48b2c940565e05cc3d1` for the once-upon-a-time goals & roadmap.  
> Development has diverged from roadmap, though goals are intact, path will differ.
>  
> ...and this readme file and codebase is a chaorgic mess,  
>	like a jackdaw's nest of shell code and philosophizing

# quickstart (installation goals)...
_(**workflow is not yet fully functioning as written here, use instructions to obtain / activate below**)_

...in an ideal world,  would look something like  
> `bash <(curl -s https://functions.sh/im)` <-- probably needs SHA sum checking attached to this
>  
> This executes the bootstrapper in a subshell,  
you can start using `f` commands in new shells immediately.
> 
> Or,  
> if you're the cautious type,  
> you can clone, build, and install it after verifying the hash (see [doc/readme.md]).
>


This repo is mostly about providing easy access to the frustratingly convoluted portions of personal system administration...with a decent emphasis on reducing context-switching time...  
  
...though ultimately, this is a **bash aliases / functions handler**, and should be treated carefully as such.


# obtain
Get the shim:
`wget https://functions.sh -o shim.src`  

> OR  
> Clone the repo from `user@functions.sh:.`  
>	eg 'mkdir -p ~/run/ && cd ~/run && f clone user@functions.sh:. ./functions.sh`
>  
> Enter the repo:  
>	`cd functions.sh`
>  
> 

# activate

Once you have the shim file, verify it:

`@todo - look up how to semicrossplatform (mint / termux-droid / nixos) SHA sum & verify, create 1-liner for those & combine`

Then source it:
```
. ~/run/shim.src
```
(you may need to use `source` or some other method of loading on your system.)


Functionality contained in these scripts works best in conjunction with a version controlled project,  
try `fet` to display quick repository information, use `fd` to show the first diff,   
or try `ff` to jump to the directory where the functions.sh shim was installed for quickly transitioning into fsh dev work.

# use

Once you've sourced the shimfile, 




# persistence (POTENTIALLY DESTRUCTIVE)
use `shimMe` to make the shimfile semi-permanent.

@todo - append a source line to bash aliases file if found instead of moving

This will move any existing file at `~/.bash_aliases` to `~/.bash_aliases.bak`, sourcing that file if it exists, and putting a symbolic link from the contents of the current `fDir` variable in place of the aforementioned file.

In case that was as difficult to follow as it was to write, you'll end up with a symbolic link from `~/.bash_aliases` to wherever you've got `shim.src` stashed away, and any previous aliases will override any fsh defaults, to preserve customization continuity. (<-- this is why the movement, because appending to the top of a file is apparently way harder than appending to the end, and there's enough bash-differentiation between systems that simply appending to the second line is not a good idea either. compromises...)

You can also add a line sourcing the shim file within you `.bashrc / .bash_profile` above the conditional for loading the aliases (do `grep -rin bash_aliases ~/` to find the location on your system, if any).

Easiest and most direct path is to not have an existing aliases file (current version of `shimMe` works as intended in this case) but being an edge case and unlikely for anyone willing to risk installing this piece of hackery, this entire section is basically a disclaimer saying "...don't (yet.)"

Thanks for reading.

## Things Which May Work 
### (currently-2022.Q1-ish)

Loading the shim via `. ./shim.src` or similar from within the repository root.  
Loading the shim via symlink or source line in existing part of profile.

Viewing version control summary (`fet`)  
	(requires shim to be loaded and git (among other operational dependencies, it's bring-your-own-dependency-heck rn) to be installed.)

functions, somewhat working  
 - `when` <-- displays chronostamp, formatted information about local system time
 
 - `logThis("message text")` <-- logs your message to default location,  
	(change destination with a second parameter)  

 - `shimMe` <-- links the currently-loaded shim via your `.bash_aliases` file. (unstable, indev)
 - `unShimMe` <-- puts it back like we found it, uninstall.



## Things Which Most Likely Don't Work

using camelCase for variables & lowercase for functions is an ideal, with low adoption, and needs refining

The kanbash cluster ing & default config settings works-ish, but requires manual install and config of [kanban.bash](https://github.com/coderofsalvation/kanban.bash) and is a hard override, so.
Task tracking was going to migrate over to taskwarrior (from kanban.bash), but that is being reversed.


The `xtrace` utility may break everything, or it may work for your use case. Know what you're using before you delve.

# persistence (old)

The shim, as installed by the quickstart command, allows the use of aliases (see `core/aliases.sh`) and most `fsh/tool` scripts, but worry-free persistence needs the build service installed, and many commands are for system administration, eg `shutgrade`.

> Build service installation is a whole kettle of worms, and needs a proper install process hacked out.


# scripting
Include `f.sh` in your scripts with:  
`shopt -s expand_aliases; . path/to/f.sh`  
or execute piecemeal as described in [#quickstart](#quickstart)

# legal
all contributions assumed original & freely given,  
non-commercial/hobbiest/educational use encouraged.

## knowledge disclaimer
All of this came from a book or a website somewhere,  
if you believe you work has been referenced,  
thank you for teaching that particular trick.

More than willing to add you to the list of referenced code, please feel free to open a pull request.

For purposes of branding & legality,  
`f.sh`/`fsh`/`f` &copy; 1990-present [@jakimfett](https://jakimfett.com/).

Due to the resurgence of public toleration for neo-nazi identities,  
authoritarian/nationalist/colonial use explicitly prohibited,  
go kick rocks in flip flops, ya gorram fascists.

# commercial support
Contact '[via email](mailto:office@assorted.tech)' or  '[web form](https://assorted.tech/contact)' for commercial licensing & monetization use.



# old usage (for posterity)
Include functionality in your own scripts with:
```
#!/
DIR=path/to/fsh/
[[ -f $DIR/$1  ]] || { echo "fsh not found"; exit 1; }
[[ "$1" =~ ../ ]] && { echo "Forbidden path"; exit 2; }
source /usr/local/etc/library.sh
cd $DIR
launch_script $1
```
@todo - add curl etc options & POST processing to allow selection of features baked into the shim (e.g. full / lite / no-network / etc)
